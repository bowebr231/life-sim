/**
 * Sim Object that represents an obstacle (rock).
 */
public class Obstacle
{
    private int o_Xpos;
    private int o_Ypos;
    private double o_diameter;
    private int o_height;
    private int o_id;


    /**
     * Constructor
     * @param o_Xpos
     * @param o_Ypos
     * @param o_diameter
     * @param o_height
     * @param o_id
     */
    public Obstacle(int o_Xpos,int o_Ypos, double o_diameter, int o_height,int o_id)
    {
        this.o_Xpos = o_Xpos;
        this.o_Ypos = o_Ypos;
        this.o_diameter = o_diameter;
        this.o_height = o_height;
        this.o_id = o_id;

    }

    /**
     * Getter
     * @return X Position
     */
    public int getXpos()
    {
        return o_Xpos;

    }

    /**
     * Getter
     * @return Y Position
     */
    public int getYpos()
    {
        return o_Ypos;

    }

    /**
     * Getter
     * @return obstacle diameter
     */
    public double getO_diameter()
    {
        return o_diameter;
    }

    /**
     * Getter
     * @return obstacle unique id
     */
    public int getO_id()
    {
        return o_id;
    }

}
