import java.util.Timer;

/**
 * This class holds functions that GUIController calls, and initializes many classes used in the program.
 */
public class UI
{
    Timer timer;
    Clock task;

    Map map;
    LocationSquare[][] newMap;
    DataBase db;
    AnimalFactory factory;
    GameMaster gameMaster;


    // default constructor
    public UI()
    {
        System.out.println("UI constructor");
        gameMaster = GameMaster.getInstance();
        map = new Map(100,100);
        newMap = map.getInstance();
        newMap[1][1].appendFlag('z');
        db = DataBase.getInstance();


        timer = new Timer();
        task = new Clock();
        timer.scheduleAtFixedRate(task, 0, 1000);

        factory = AnimalFactory.getInstance();
        factory.CreatePredator();
        factory.CreateGrazer();

    }

    // prints reports to out file when chosen
    public void PrintReports()
    {
        db.CreateReportDoc(task);
        System.out.println("Prints reports");

    }

    /**
     * Controls the run speed of the sim by stoping the current task after it is complete
     * and starting a new task with a different run speed
     * @author Bradley Washburn
     */
    public void speeds1x()
    {
        timer.cancel();
        timer = new Timer();
        task = new Clock();
        timer.scheduleAtFixedRate(task, 0, 1000);
        // System.out.println("Clock 1x");
    }
    /**
     * Controls the run speed of the sim by stoping the current task after it is complete
     * and starting a new task with a different run speed
     * @author Bradley Washburn
     */
    public void speeds10x()
    {
        timer.cancel();
        timer = new Timer();
        task = new Clock();
        timer.scheduleAtFixedRate(task, 0, 100);
        //System.out.println("Clock 10x");
    }
    /**
     * Controls the run speed of the sim by stoping the current task after it is complete
     * and starting a new task with a different run speed
     * @author Bradley Washburn
     */
    public void speeds50x()
    {
        timer.cancel();
        timer = new Timer();
        task = new Clock();
        timer.scheduleAtFixedRate(task, 0, 20);
        //System.out.println("Clock 50x");
    }
    /**
     * Controls the run speed of the sim by stoping the current task after it is complete
     * and starting a new task with a different run speed
     * @author Bradley Washburn
     */
    public void speeds100x()
    {
        timer.cancel();
        timer = new Timer();
        task = new Clock();
        timer.scheduleAtFixedRate(task, 0, 10);
        //System.out.println("Clock 100x");
    }
}
