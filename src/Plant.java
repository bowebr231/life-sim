import java.util.*;

/**
 * Sim Object that represents an plant.
 */
public class Plant
{
    Random random = new Random();
    Clock clock = new Clock();

    DataBase dataBase = DataBase.getInstance();
    private int p_Xpos;
    private int p_Ypos;
    private double p_diameter;
    private double p_radius;
    public final double GROWTH_RATE;
    private final int MAX_SIZE;
    private final int MAX_SEEDS ;
    private final int MAX_CAST ;
    private double p_viability;
    private boolean p_isViable;
    private boolean p_isSeed;
    public boolean isMAX = false;
    private int p_id;
    public static int  nums = 30;
    public int startGrowthCounter =0;
    private int produceSeedsEachHourCounter = 0; //In units of seconds


    /**
     * Plant Constructor
     * @param GROWTH_RATE   Rate at which plants grow
     * @param MAX_SIZE      Max Possible Size
     * @param MAX_SEEDS     Max Seeds to cast
     * @param MAX_CAST      Max Cast Distance
     * @param p_viability   Chance of sprouting
     * @param p_Xpos        x position
     * @param p_Ypos        y position
     * @param p_diameter    current diameter
     * @param p_id          personal plant ID
     */
    public Plant(
                 double GROWTH_RATE,int MAX_SIZE,
                 int MAX_SEEDS, int MAX_CAST,
                 double p_viability, int p_Xpos,
                 int p_Ypos,double p_diameter,int p_id
                )

    {
       this.GROWTH_RATE = GROWTH_RATE;
       this.MAX_SIZE = MAX_SIZE;
       this.MAX_SEEDS = MAX_SEEDS;
       this.MAX_CAST = MAX_CAST;
       this.p_viability = p_viability;
       this.p_Xpos = p_Xpos;
       this.p_Ypos = p_Ypos;
       this.p_diameter = p_diameter;
       this.p_id = p_id;
    }

    /**
     * Primary function called from GameMaster.tick() that performs all updates for a single plant
     */
    public void updatePlant() {

        //Don't start growing until 10 seconds have occured (except for original xml plants)
        if(startGrowthCounter <10)
        {
            startGrowthCounter++;

        } else if (this.isMAX == false) { // Until max size, grow every tick.
            this.Grow();

        } else {
            produceSeedsEachHourCounter++; // Track time before seeds should be produced
        }

        if(produceSeedsEachHourCounter >= 3600) { // 3600 = an hour in seconds
            this.ProduceAndCastSeeds();

            // reset counter to wait another hour
            produceSeedsEachHourCounter = 0;
        }
    }

    /**
     * Grows plant diameter, and keeps track of its max size.
     */
    public void Grow()
    {
        if (this.getP_diameter() < MAX_SIZE) {
            this.isMAX = false;

            //this.p_diameter += GROWTH_RATE;
            double result= MAX_SIZE * GROWTH_RATE;
            this.p_diameter += result;

            //GROWTHRATE was 0.1 now 0.01

        } else {
            this.isMAX = true;
            //System.out.println(this.p_id + " stop ima grown ups");
            // System.out.println(this.p_id + " stop ima grown ups");
        }
    }

    /**
     * Creates random number of viable seeds, determines cast distance and location,
     * and creates the instances of the plants.
     */
    public void ProduceAndCastSeeds()
    {
        //Create random number of seeds
        int randomNumofSeeds = random.nextInt(MAX_SEEDS);

        //Determine which percentage of seeds will actually grow (aka. be produced)
        int viableRandomNumofSeeds = 0;
        for(int i = 0; i < randomNumofSeeds; i++) {
            //Calculate a random direction to throw in the X and Y directions
            double randomNum = random.nextDouble(); // returns a random number 0.0 to 1.0
            if (randomNum < p_viability) { //viability is a decimal percentage. Currently 0.5
                viableRandomNumofSeeds++;
            }
        }

        //Used to get bounds of map
        LifeSimDataParser parser = LifeSimDataParser.getInstance();

        //Debug
       // nums++;
       // System.out.println("plant " + this.getP_id() + " Cast " + Q + " seeds");

        //Calculate location seeds are cast to
        for(int z = 0; z < viableRandomNumofSeeds; z++) {

            //Calculate a random cast distance
            double castDistX = random.nextInt(MAX_CAST);
            double castDistY = random.nextInt(MAX_CAST);

            //Calculate a random direction to throw in the X and Y directions
            double isNeg = random.nextDouble();
            if (isNeg < 0.51) {
                castDistX = castDistX * -1;
            }
            isNeg = random.nextDouble();
            if (isNeg < 0.51) {
                castDistY = castDistY * -1;
            }

            //Prevent seeds from being thrown out-of-bounds of the grid
            //Handle X coordinate
            double xpos = this.getX_pos() + castDistX;
            if (xpos > parser.getWorldWidth()) {
                xpos = parser.getWorldWidth() - 5;
            }
            if (xpos <= 0) {
                xpos = 5;
            }
            //Handle Y coordinate
            double ypos = this.getY_pos() + castDistY;
            if (ypos > parser.getWorldHeight()) {
                ypos = parser.getWorldHeight() - 5;
            }
            if (ypos <= 0) {
                ypos = 5;
            }

            //Create the new plant (seed), and then add it to the database list
            Plant newPlant = new Plant(GROWTH_RATE,MAX_SIZE,MAX_SEEDS,MAX_CAST,p_viability,(int)xpos,(int)ypos,
            2.0/100.0, nums++);
            //System.out.println("plant " + this.getP_id() + " Cast " + viableRandomNumofSeeds + " seeds    " + " new plant id's: " + newPlant.getP_id() + " X: " + newPlant.getX_pos() + "Y: " + newPlant.getY_pos());
            dataBase.plantList.add(newPlant);
        }
    }

    public int getX_pos() {
        return p_Xpos;
    }

    public int getY_pos() {
        return p_Ypos;
    }

    public double getP_diameter() {

        return p_diameter;
    }

    public int getP_id()
    {
        return p_id;
    }
}
