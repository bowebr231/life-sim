import java.util.ArrayList;
import java.util.Random;

import static java.lang.Math.abs;

import java.util.*;
/**
 * Calculates outcomes and behaviors for all living objects within the simulation
 */
public class GameMaster
{
    public String DATAFILE = new String(System.getProperty("user.dir") + "/LifeSimulation01.xml");
    public LifeSimDataParser import_data = LifeSimDataParser.getInstance();

    private static GameMaster gameMaster;
    private  DataBase dataBase;
    private ArrayList<Grazer> grazers;
    private ArrayList<Predator> predators;
    private ArrayList<Plant> plants;
    int Plantcount;

    /**
     * Singleton pattern
     * @return gameMaster
     */
    public static GameMaster getInstance()
    {
        if (gameMaster == null)
            gameMaster = new GameMaster();
        return gameMaster;
    }

    private GameMaster() {
        gameMaster = this;
        Plantcount=0;
        dataBase = DataBase.getInstance();
        import_data.initDataParser(DATAFILE);
        Map initialize_map = new Map(import_data.getWorldHeight(), import_data.getWorldWidth());


        //Initialize all plants for the sim
        for (int count = 0; count < import_data.getInitialPlantCount(); count++) {
            import_data.getPlantData();
            Plant initialize_plant = new Plant(
                            import_data.getPlantGrowthRate(),
                            import_data.getMaxPlantSize(),
                            import_data.getMaxSeedNumber(),
                            import_data.getMaxSeedCastDistance(),
                            import_data.getSeedViability(),
                            import_data.PlantX,import_data.PlantY,
                            import_data.PlantDiameter, count
                    );
            initialize_plant.startGrowthCounter = 10; //Set so first set of plants from xml begin growing immediately.
            dataBase.PlantCount++;
            dataBase.plantList.add(initialize_plant);

        }





    for(int count = 0; count < import_data.getObstacleCount(); count++)
    {
        import_data.getObstacleData();
        Obstacle initializeObstacle = new Obstacle(
                        import_data.ObstacleX,
                        import_data.ObstacleY,
                        import_data.ObstacleDiameter,
                        import_data.ObstacleHeight,count

                );
        dataBase.obstacleList.add(initializeObstacle);
        dataBase.obstacleCount++;

    }
        this.grazers = dataBase.getV_grazerList();
        this.predators = dataBase.getV_pedatorList();
        this.plants = dataBase.getV_plantList();
    }

    /**
     * @param animal this is a reference to the animal that called the method
     *               this method adds all plants within vision_du of the animal to its food list.
     * @author d'Andre
     */
    public void UpdateFood(Grazer animal) {
        animal.visible_food.clear();
        ArrayList<Plant> databasePlants = dataBase.getV_plantList();
        for (int i = 0; i < databasePlants.size(); i++) {
            if ((abs(databasePlants.get(i).getX_pos() - animal.getX_pos()) < animal.vision_DU && abs(databasePlants.get(i).getY_pos() - animal.getY_pos()) < animal.vision_DU)) {
                if(!(animal.visible_food.contains(databasePlants.get(i))))
                {
                    animal.visible_food.add(databasePlants.get(i));
                }

            }
        }
    }

    /**
     * @param animal predator whos visible food list is being updated.
     * @author d'Andre Clifton
     * <p>
     * this function will add grazers/Predators to the appropriately aggressive predators list of food.
     * it will add only grazers to 'ss' genotype predators
     */

    private void UpdateFood(Predator animal) {
        animal.visible_food.clear();
        char[] animal1POW, animal2POW;
        int sum = 0;
        int geneSum = 0; // sums to hold ascii values
        animal1POW = animal.getP_power(); //getting pred genes

        String hunterGenes = new String(animal1POW); //converting to string, prob didnt need to do this step, but here we are

        for (int i = 0; i < 2; i++) {

            char characters1 = hunterGenes.charAt(i);

            int animalGeneInt = (int) characters1;

            geneSum = geneSum + animalGeneInt;
        } //end of gene to int conversion


        //Add Preds to a preds food list
        ArrayList<Predator> databasePreds = dataBase.getV_pedatorList();
        if (geneSum == 166 || geneSum == 198) {
            for (int i = 0; i < databasePreds.size(); i++) {
                if (abs(databasePreds.get(i).getX_pos() - animal.getX_pos()) < animal.vision_DU && abs(databasePreds.get(i).getY_pos() - animal.getY_pos()) < animal.vision_DU) {
                   if(databasePreds.get(i).UID!=animal.UID)
                   {
                       animal.visible_food.add(databasePreds.get(i));
                   }

                }
            }

        }

        //Add Visible Grazers to the preds visible food list
        ArrayList<Grazer> databaseGrazers = dataBase.getV_grazerList();
        for (int i = 0; i < databaseGrazers.size(); i++) {
            if (abs(databaseGrazers.get(i).getX_pos() - animal.getX_pos()) < animal.vision_DU && abs(databaseGrazers.get(i).getY_pos() - animal.getY_pos()) < animal.vision_DU) {
                animal.visible_food.add(databaseGrazers.get(i));
            }
        }


        //updating mating list section
        for(int i = 0; i < databasePreds.size(); i++) //adding find mate functionality here because it is functionally similar to finding food
        {
            animal.matingList.clear();
            if(abs(databasePreds.get(i).getX_pos() - animal.getX_pos()) < animal.vision_DU && abs(databasePreds.get(i).getY_pos() - animal.getY_pos()) < animal.vision_DU)
            {
                if(databasePreds.get(i).energy_level > databasePreds.get(i).ENERGY_TO_REPRODUCE)
                {
                    if(animal.UID!=databasePreds.get(i).UID)
                    {
                        animal.matingList.add(databasePreds.get(i));
                    }

                }

            }

        }


        for(int j = 0; j < animal.visible_food.size(); j++)
        {
            if( animal.visible_food.get(j).X_dst > animal.getX_pos() + animal.vision_DU || animal.visible_food.get(j).Y_dst > animal.getY_pos() + animal.vision_DU )
            {
                animal.visible_food.remove(j);
            }
        }

    }

    public boolean IsHidden(Animal animal1, Animal animal2) {

        return true;
    }

    private void Reproduce() {

    }

    /**
     * this fucntion will calculate the odds of predator vs predator combat
     *
     * @param hunter animal initiating combat
     * @param target target animal
     * @return returns true at the end of hunt, should never return false
     * <p>
     * this method will turn the char array that represents the pred's aggression gene
     * and converts it to a number. It then uses those numbers in place of string comparison
     * for the remainder of the function.
     * @author d'Andre Clifton
     */


    public boolean computeCombat(Predator hunter, Predator target)
    {
        char[] SS = {'S', 'S'};
        char[] Ss = {'S', 's'};
        char[] ss = {'s','s'};
        Random random = new Random();
        if (Arrays.equals(hunter.getP_power(), SS)) {
            if (Arrays.equals(target.getP_power(),SS))
            {
                int outcome = random.nextInt(100) + 1;
                if (outcome < 50) {
                    consume(hunter, target);
                    return true;
                } else {
                    //ignore target
                    return true;
                }

            } else if (Arrays.equals(hunter.getP_power(), Ss))
            {
                //75% chance to win
                int outcome = random.nextInt(100) + 1;
                if (outcome < 75) {
                    consume(hunter, target);
                    return true;
                } else {
                    //ignore target
                    return true;
                }
            } else {
                //95% chance to win
                int outcome = random.nextInt(100) + 1;
                if (outcome < 95) {
                    consume(hunter, target);
                    return true;
                } else {
                    //ignore target
                    return true;
                }
            }
        }

        return false;
    }

    public boolean computeCombat(Predator hunter, Grazer grazer) {
        Random random = new Random();
        char[] SS = {'S', 'S'};
        char[] Ss = {'S', 's'};
        char[] ss = {'s','s'};
        if (Arrays.equals(hunter.getP_power(), SS)) {
            //95%
            int outcome = random.nextInt(100) + 1;
            if (outcome < 95) {
                consume(hunter, grazer);
                return true;
            } else {
                //ignore targets
                return true;
            }
        } else if (Arrays.equals(hunter.getP_power(), Ss))
        {
            int outcome = random.nextInt(100) + 1;
            if (outcome < 75) {
                consume(hunter, grazer);
                return true;

            } else {
                //ignore target
                return true;
            }
        } else {
            //50%
            int outcome = random.nextInt(100) + 1;
            if (outcome < 50) {
                consume(hunter, grazer);
                return true;
            } else {
                //ignore target
                return true;
            }
        }
    }

    /**
     * This is the function that runs the animals logic
     */
    public void tick()
    {
        //Update plants
        for (int j = 0; j < dataBase.getV_plantList().size(); j++) {
            dataBase.getV_plantList().get(j).updatePlant();
        }

        for (Grazer grazer:grazers)         //TODO This also randomly breaks
        {
            UpdateFood(grazer);
        }
        for(int i = 0; i < predators.size(); i++) //this loop is for removing food from predator lists
        {
            UpdateFood(predators.get(i));

            for(int j =0; j < predators.get(i).visible_food.size();j++)
            {
                if(predators.get(i).visible_food.get(j).Dead == true)
                {
                    Animal temp = predators.get(i).visible_food.get(j); //added to avoided out of bounds index exception for below
                    predators.get(i).visible_food.remove(j);

                    if(temp == predators.get(i).target) //if the predator was targeting this animal when removed, it will reevaluate its behavior
                    {
                        predators.get(i).behavior = "default";
                    }
                }
            }
        }

        //insert code to remove dead plants from Grazers here

        for(int i = 0; i < predators.size(); i++)
        {
            predators.get(i).behavior();
        }

        for(int i = 0; i < grazers.size(); i++)
        {
            grazers.get(i).behavior();
        }

    }


    private void consume(Predator hunter, Animal target)
    {
        hunter.energy_level += target.energy_level * .90;
        target.Dead = true;
        hunter.behavior();
        //System.out.println("CONSUME");
    }
}
