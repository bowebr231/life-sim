public class Map
{
    /**
     * The Map class is a singleton class that creates and returns an instance of the game map< not working.
     */
    double  worldWidth;
    double  worldHeight;
    private static LocationSquare[][] mapSingleton;

    public Map(double worldHeight,double worldWidth)
    {
        this.worldHeight = worldHeight;
        this.worldWidth = worldWidth;
    }

    public LocationSquare[][] getInstance()
    {
        if(mapSingleton == null)
        {
            mapSingleton = new LocationSquare[(int)worldWidth/25][(int)worldHeight/25];
            for(int i = 0; i < worldWidth/25; i++)
            {
                for(int j = 0; j < worldHeight/25; j++)
                {
                    mapSingleton[i][j] = new LocationSquare(i,j);
                }
            }
            return mapSingleton;
        }

        if(mapSingleton != null)
            return mapSingleton;

        return null;//should never return null, ever.
    }
}

