import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

/**
 * GUI class that draws on and controls the GUI.
 * @author Bradley Bowen
 */
public class GUIController {
    //References
    /** GUI reference to the UI class */
    private UI m_uI;
    /** Reference to the database */
    private final DataBase m_dataBase;
    /** Global reference for other objects to make calls to this controller class */
    public static GUIController GUIControllerReference;

    //Flags
    /** Flag that prevents clock from calling GUIController functions before its ready */
    public boolean m_readyForClock;
    /** Flag that prevents actions from occurring in clock */
    public boolean m_isPaused;
    /** Grid drawn flag to prevent multiple redraws */
    private boolean m_notDrawn;
    /** Flag used to prevent start button and others from performing actions after sim has started */
    private boolean m_simHasStarted;

    //Image constants
    /** Holds an instance of the Obstacle image that will be used in all image instances */
    private final Image m_IMAGE_OBSTACLE;
    /** Holds an instance of the Plant image that will be used in all image instances */
    private final Image m_IMAGE_PLANT;
    /** Holds an instance of the Grazer image that will be used in all image instances */
    private final Image m_IMAGE_GRAZER;
    /** Holds an instance of the Predator image that will be used in all image instances */
    private final Image m_IMAGE_PREDATOR;
    /** Holds an instance of the Error image that will be used in all image instances */
    private final Image m_IMAGE_ERROR;

    //Currently displayed images list
    /** Lists of all Obstacle images currently displayed in the SimWindow */
    private final ArrayList<SimObjectView> m_displayedObstaclesList;
    /** Lists of all Plant images currently displayed in the SimWindow */
    private final ArrayList<SimObjectView> m_displayedPlantsList;
    /** Lists of all Grazer images currently displayed in the SimWindow */
    private final ArrayList<SimObjectView> m_displayedGrazersList;
    /** Lists of all Predator images currently displayed in the SimWindow */
    private final ArrayList<SimObjectView> m_displayedPredatorsList;

    //Hard-coded values
    /** Hard-coded value that determines how much vertical space is between each line on display grid. */
    private final double m_GRID_Y_INCREMENT;
    /** Hard-coded value that determines how much horizontal space is between each line on display grid. */
    private final double m_GRID_X_INCREMENT;

    //Offset variables
    /** Values used to space items on the grid in correct Y positions. */
    private double m_gridYSpaceOffset;
    /** Values used to space items on the grid in correct X positions. */
    private double m_gridXSpaceOffset;

    /** The simulation window that has the grid. Object reference imported from FXML */
    @FXML
    private Pane m_SimWindow;
    /** Label that displays current clock time. Object reference imported from FXML */
    @FXML
    public Label m_ClockText;
    /** Label that displays current status of Sim execution */
    @FXML
    private Label m_SimStatusExecution;
    /** Label that displays current status of Clock Speed */
    @FXML
    private Label m_SimStatusClockSpeed;
    /** Pause button for changing the button text */
    @FXML
    private Button m_PauseButton;

    /**
     * Types used to select correct image for object
     */
    public enum SimType {
        OBSTACLE,
        PLANT,
        GRAZER,
        PREDATOR
    }

    /**
     Constructor
     */
    public GUIController() throws FileNotFoundException {
        m_uI = null;
        GUIControllerReference = this;
        m_dataBase = DataBase.getInstance();

        m_isPaused = false;
        m_notDrawn = true;
        m_simHasStarted = false;
        m_readyForClock = false;

        m_displayedObstaclesList = new ArrayList<>();
        m_displayedPlantsList = new ArrayList<>();
        m_displayedGrazersList = new ArrayList<>();
        m_displayedPredatorsList = new ArrayList<>();

        //initialize all image member variable constants.
        FileInputStream inputStream = new FileInputStream("src/Pictures/Obstacle.png");
        m_IMAGE_OBSTACLE = new Image(inputStream);
        inputStream = new FileInputStream("src/Pictures/Plant.png");
        m_IMAGE_PLANT = new Image(inputStream);
        inputStream = new FileInputStream("src/Pictures/Grazer.png");
        m_IMAGE_GRAZER = new Image(inputStream);
        inputStream = new FileInputStream("src/Pictures/Predator.png");
        m_IMAGE_PREDATOR = new Image(inputStream);
        inputStream = new FileInputStream("src/Pictures/Error.png");
        m_IMAGE_ERROR = new Image(inputStream);

        m_GRID_Y_INCREMENT = 25;
        m_GRID_X_INCREMENT = 25;

        m_gridYSpaceOffset = 0;
        m_gridXSpaceOffset = 0;
    }

    /**
     * Updates simulation display. Called each time increment. Called by GameMaster.
     */
    public void DisplaySim() {

        //Draw Grid first time
        if (m_notDrawn) {
            drawGrid();
            displayObstacles(); //Only has to be added/updated to screen once, I think.
            m_notDrawn = false;
        }

        //Update screen with new object data
        updatePlants();
        updateGrazers();
        updatePredators();

        updateClockText();

        //debugPrintIDs();
        // System.out.println("DisplaySim() was called.");
    }

    /**
     * Draws grid.
     */
    public void drawGrid () {
        //Get SimWindow dimensions
        double paneHeight = m_SimWindow.getHeight();
        double paneWidth = m_SimWindow.getWidth();

        //Currently hard-coded. Will call values directly from database when getter exists.
        LifeSimDataParser xmlParser = LifeSimDataParser.getInstance();
        final double xml_LAND_BOUNDS_Y = xmlParser.getWorldHeight(); //original was 750
        final double xml_LAND_BOUNDS_X = xmlParser.getWorldWidth(); //original was 1000
        //Number of gridlines.
        int numOfGridVerticalLines = (int) (xml_LAND_BOUNDS_X / m_GRID_X_INCREMENT); //Should be 30 lines if height is 750
        int numOfGridHorizontalLines = (int) (xml_LAND_BOUNDS_Y / m_GRID_Y_INCREMENT); //Should be 40 lines if width is 1000

        //Debug Code
        //System.out.println("#Vertical lines: "+ numOfGridVerticalLines);
        //System.out.println("#Horizontal lines: "+ numOfGridHorizontalLines);

        //Leaves space for coordinate numbers
        m_gridYSpaceOffset = ((int) paneHeight - xml_LAND_BOUNDS_Y) / 2; //Assumed pane is larger than xmlCoords
        m_gridXSpaceOffset = ((int) paneWidth - xml_LAND_BOUNDS_X) / 2;

        //Draw Grid lines
        //Vertical lines
        double xPosition = 0;
        //Starting number
        double coordNumber = 0;
        for (int i = 0; i < numOfGridVerticalLines + 1; i++) {

            Line line = new Line();
            //Line color
            line.setFill(Color.LIGHTGRAY);
            line.setStroke(Color.LIGHTGRAY);
            //Line Positions
            line.setStartX(xPosition + m_gridXSpaceOffset);
            line.setStartY( 0 + m_gridYSpaceOffset);
            line.setEndX(xPosition + m_gridXSpaceOffset);
            line.setEndY(xml_LAND_BOUNDS_Y + m_gridYSpaceOffset);

            //Coordinate numbers text
            Label coordNumTextTop = new Label(String.valueOf((int) coordNumber));
            Label coordNumTextBottom = new Label(String.valueOf((int) coordNumber));
            //Set text font size
            coordNumTextTop.setFont(new Font("Arial Bold", 10));
            coordNumTextBottom.setFont(new Font("Arial Bold", 10));
            //Coordinate numbers text position
            coordNumTextTop.setTranslateX(xPosition + m_gridXSpaceOffset - 5);
            coordNumTextTop.setTranslateY(m_gridYSpaceOffset - 15);
            coordNumTextBottom.setTranslateX(xPosition + m_gridXSpaceOffset - 5);
            coordNumTextBottom.setTranslateY(xml_LAND_BOUNDS_Y + m_gridYSpaceOffset + 10);

            //Add to SimWindow
            m_SimWindow.getChildren().add(line);
            m_SimWindow.getChildren().add(coordNumTextTop);
            m_SimWindow.getChildren().add(coordNumTextBottom);

            //Move to next line position
            xPosition += m_GRID_X_INCREMENT;
            //Increment number
            coordNumber += m_GRID_Y_INCREMENT;

        }
        //Horizontal lines
        double yPosition = 0;
        //Reset coordinate number
        coordNumber = 0;
        for (int i = 0; i < numOfGridHorizontalLines + 1; i++) {

            Line line = new Line();
            //Line color
            line.setFill(Color.LIGHTGRAY);
            line.setStroke(Color.LIGHTGRAY);
            //Line Positions
            line.setStartX(0 + m_gridXSpaceOffset);
            line.setStartY(yPosition + m_gridYSpaceOffset);
            line.setEndX(xml_LAND_BOUNDS_X + m_gridXSpaceOffset);
            line.setEndY(yPosition + m_gridYSpaceOffset);

            //Coordinate numbers text
            Label coordNumTextLeft = new Label(String.valueOf((int) coordNumber));
            Label coordNumTextRight = new Label(String.valueOf((int) coordNumber));
            //Set text font size
            coordNumTextLeft.setFont(new Font("Arial Bold", 10));
            coordNumTextRight.setFont(new Font("Arial Bold", 10));
            //Coordinate numbers text position
            coordNumTextLeft.setTranslateX(m_gridXSpaceOffset - 20);
            coordNumTextLeft.setTranslateY(yPosition + m_gridYSpaceOffset - 5);
            coordNumTextRight.setTranslateX(xml_LAND_BOUNDS_X + m_gridXSpaceOffset + 10);
            coordNumTextRight.setTranslateY(yPosition + m_gridYSpaceOffset - 5);

            //Add to SimWindow
            m_SimWindow.getChildren().add(line);
            m_SimWindow.getChildren().add(coordNumTextLeft);
            m_SimWindow.getChildren().add(coordNumTextRight);

            //Move to next line position
            yPosition += m_GRID_Y_INCREMENT;
            //Increment number
            coordNumber += m_GRID_Y_INCREMENT;
        }
    }

    /**
     * Creates new sim object to be displayed on screen.
     * @param xPos X Position
     * @param yPos Y position
     * @param objectType Obstacle, Plant, Grazer, Predator
     * @param simObjectID unique ID that identifies the object. Same as the real object ID
     */
    public void addObjectToScreen(double xPos, double yPos, SimType objectType, int simObjectID, double diameter) {
        //If object hasn't already been added to the screen
        if(findObjectImage(simObjectID, objectType) == null) {
            SimObjectView simObjectView;

            //Create a new SimObjectView object to be displayed
            switch (objectType) {
                case OBSTACLE:
                    simObjectView = new SimObjectView(m_IMAGE_OBSTACLE, simObjectID);
                    //Add to current list of objects for tracking
                    m_displayedObstaclesList.add(simObjectView);
                    break;

                case PLANT:
                    simObjectView = new SimObjectView(m_IMAGE_PLANT, simObjectID);
                    //Add to current list of objects for tracking
                    m_displayedPlantsList.add(simObjectView);
                    break;

                case GRAZER:
                    simObjectView = new SimObjectView(m_IMAGE_GRAZER, simObjectID);
                    //Add to current list of objects for tracking
                    m_displayedGrazersList.add(simObjectView);
                    break;

                case PREDATOR:
                    simObjectView = new SimObjectView(m_IMAGE_PREDATOR, simObjectID);
                    //Add to current list of objects for tracking
                    m_displayedPredatorsList.add(simObjectView);
                    break;

                default:
                    simObjectView = new SimObjectView(m_IMAGE_ERROR, simObjectID);
            }

            //Resizes picture to its specified parameter
            simObjectView.setFitHeight(diameter);
            simObjectView.setFitWidth(diameter);

            //Calculate the center coordinate of the image
            double imageWidth = simObjectView.getFitWidth();
            double imageHeight = simObjectView.getFitHeight();
            double centeredX = ((imageWidth + xPos) + xPos) / 2; //Uses Midpoint of a Line Segment math formula
            double centeredY = ((imageHeight + yPos) + yPos) / 2; //Uses Midpoint of a Line Segment math formula
            double differenceX = centeredX - xPos;
            double differenceY = centeredY - yPos;
            double finalX = xPos - differenceX;
            double finalY = yPos - differenceY;

            //Moves object to specified position
            simObjectView.setTranslateX(finalX + m_gridXSpaceOffset);
            simObjectView.setTranslateY(finalY + m_gridYSpaceOffset);

            //Adds image to window pane
            m_SimWindow.getChildren().add(simObjectView);
        }
        else {
            System.err.println("Life-Sim Error: Function addObjectToScreen(...) was called. Object with id '"+simObjectID+"-"+objectType+"' has already been added to the screen.");
            //Platform.exit();
        }
    }

    /**
     * Displays all Obstacle objects in database to the screen.
     */
    public void displayObstacles() {
        //Add Obstacles
        //Reference to current object
        Obstacle dbObject;
        // Might need update for resize but that's it.
        for(int i = 0; i < m_dataBase.getV_obstacleList().size(); i++) {

            dbObject = m_dataBase.getV_obstacleList().get(i);
            SimObjectView guiObjectImage = findObjectImage(dbObject.getO_id(), SimType.OBSTACLE);

            //if new object
            if( guiObjectImage == null) { //search didn't find id in gui list.
            addObjectToScreen(dbObject.getXpos(),
                    dbObject.getYpos(), SimType.OBSTACLE, dbObject.getO_id(), dbObject.getO_diameter());
            }
        }

        //  System.out.println("DatabaseObstacles#: " + dataBase.getV_obstacleList().size());
        // System.out.println("GuiObstacles#: " + m_displayedObstaclesList.size());
        //  System.out.println();
    }

    /**
     * Displays all Plant objects in database to the screen, including their growth.
     */
    public void updatePlants() {

        //Reference to current object
        Plant dbObject;
        //List keeps up with what images to remove from screen
        ArrayList<SimObjectView> removeImageList = (ArrayList<SimObjectView>) m_displayedPlantsList.clone();

        //Update Plants
        for(int i = 0; i < m_dataBase.getV_plantList().size(); i++) {
            //addObjectToScreen(dataBase.getV_plantList().get(i).getX_pos(),
            //dataBase.getV_plantList().get(i).getY_pos(), SimType.PLANT, dataBase.getV_plantList().get(i).getP_id(), dataBase.getV_plantList().get(i).getP_diameter());

            dbObject = m_dataBase.getV_plantList().get(i);
            SimObjectView guiObjectImage = findObjectImage(dbObject.getP_id(), SimType.PLANT);

            //if new object
            if( guiObjectImage == null) { //search didn't find id in gui list.
                addObjectToScreen(dbObject.getX_pos(),
                        dbObject.getY_pos(), SimType.PLANT, dbObject.getP_id(), dbObject.getP_diameter());
            } else { //If already on screen

                //Update plant image size based on its growth
                resizePlantImage(dbObject.getX_pos(), dbObject.getY_pos(), dbObject.getP_diameter(), dbObject.getP_id());

                //Remove object from temp list
                removeImageList.remove(guiObjectImage);
            }
        }

        //If dead or removed from simulation (aka should be removed from screen)
        for(int i = 0; i < removeImageList.size(); i++) {
            m_SimWindow.getChildren().remove(removeImageList.get(i));
        }

        //Debug code
        // System.out.println("DatabasePlants#: " + dataBase.getV_plantList().size());
        // System.out.println("GuiPlants#: " + m_displayedPlantsList.size());
        //  System.out.println();
    }

    /**
     * Displays all Grazer objects in database to the screen, and updates their displayed position.
     */
    public void updateGrazers() {
        //Reference to current object
        Grazer dbObject;
        //List keeps up with what images to remove from screen
        ArrayList<SimObjectView> removeImageList = (ArrayList<SimObjectView>) m_displayedGrazersList.clone();

        //Begin looping through database objects
        for(int i = 0; i < m_dataBase.getV_grazerList().size(); i++) {

            dbObject = m_dataBase.getV_grazerList().get(i);
            SimObjectView guiObjectImage = findObjectImage(dbObject.UID, SimType.GRAZER);

            //if new object
            if( guiObjectImage == null) { //search didn't find id in gui list.
                addObjectToScreen(dbObject.X_pos,
                        dbObject.Y_pos, SimType.GRAZER, dbObject.UID, 25);
            } else { //If already on screen

                //Update position and other visual values
                moveObjectOnScreen(dbObject.X_pos,
                        dbObject.Y_pos, SimType.GRAZER, dbObject.UID);

                //Remove object from temp list
                removeImageList.remove(guiObjectImage);
            }
        }

        //If dead or removed from simulation (aka should be removed from screen)
        for(int i = 0; i < removeImageList.size(); i++) {
            m_SimWindow.getChildren().remove(removeImageList.get(i));
        }

        //Debug code
        //System.out.println("DatabaseGrazers#: " + dataBase.getV_grazerList().size());
        //System.out.println("GuiGrazers#: " + m_displayedGrazersList.size());
        //System.out.println();
    }

    /**
     * Displays all Predator objects in database to the screen, and updates their displayed position.
     */
    public void updatePredators() {
        //Reference to current object
        Predator dbObject;
        //List keeps up with what images to remove from screen
        ArrayList<SimObjectView> removeImageList = (ArrayList<SimObjectView>) m_displayedPredatorsList.clone();

        //Begin looping through database objects
        for(int i = 0; i < m_dataBase.getV_pedatorList().size(); i++) {

            dbObject = m_dataBase.getV_pedatorList().get(i);
            SimObjectView guiObjectImage = findObjectImage(dbObject.UID, SimType.PREDATOR);

            //if new object
            if( guiObjectImage == null) { //search didn't find id in gui list.
                addObjectToScreen(dbObject.X_pos,
                        dbObject.Y_pos, SimType.PREDATOR, dbObject.UID, 25);
            } else { //If already on screen

                //Update position and other visual values
                moveObjectOnScreen(dbObject.X_pos,
                        dbObject.Y_pos, SimType.PREDATOR, dbObject.UID);
                //Remove object from temp list
                removeImageList.remove(guiObjectImage);
            }
        }

        //If dead or removed from simulation (aka should be removed from screen)
        for(int i = 0; i < removeImageList.size(); i++) {
            m_SimWindow.getChildren().remove(removeImageList.get(i));
        }

        //Debug code
        //System.out.println("DatabasePredators#: " + dataBase.getV_pedatorList().size());
        //System.out.println("GuiPredators#: " + m_displayedPredatorsList.size());
        //System.out.println();
    }

    /**
     * Looks up an object image that has already been added to the screen.
     * @param id object ID
     * @return reference to the object image. Null if can't be found
     */
    private SimObjectView findObjectImage(int id, SimType objectType) {

        SimObjectView result = null;

        if(objectType == SimType.OBSTACLE) {
            //Retrieves the object you wish to move.
            for (SimObjectView image : m_displayedObstaclesList) {
                if (image.getObjectID() == id) {
                    result = image;
                }
            }
        } else if(objectType == SimType.PLANT) {
            //Retrieves the object you wish to move.
            for (SimObjectView image : m_displayedPlantsList) {
                if (image.getObjectID() == id) {
                    result = image;
                }
            }
        } else if(objectType == SimType.GRAZER) {
            //Retrieves the object you wish to move.
            for (SimObjectView image : m_displayedGrazersList) {
                if (image.getObjectID() == id) {
                    result = image;
                }
            }
        } else if(objectType == SimType.PREDATOR) {
            //Retrieves the object you wish to move.
            for (SimObjectView image : m_displayedPredatorsList) {
                if (image.getObjectID() == id) {
                    result = image;
                }
            }
        }

        //Null if ID not found
        return result;
    }

    /**
     * Used to update an objects current displayed position on the gui grid.
     * @param xPos on grid
     * @param yPos on grid
     * @param objectType Obstacle, Plant, Grazer, or Predator SimType enum
     * @param simObjectID unique id that identifies this object image.
     */
    public void moveObjectOnScreen(double xPos, double yPos, SimType objectType, int simObjectID) {
        //Searches for object by ID.
        SimObjectView objectImage = findObjectImage(simObjectID, objectType);

        //Calculate the center coordinate of the image
        double imageWidth = objectImage.getFitWidth();
        double imageHeight = objectImage.getFitHeight();
        double centeredX = ((imageWidth + xPos) + xPos) / 2; //Uses Midpoint of a Line Segment math formula
        double centeredY = ((imageHeight + yPos) + yPos) / 2; //Uses Midpoint of a Line Segment math formula
        double differenceX = centeredX - xPos;
        double differenceY = centeredY - yPos;
        double finalX = xPos - differenceX;
        double finalY = yPos - differenceY;

        //Moves object to specified position
        objectImage.setTranslateX(finalX + m_gridXSpaceOffset);
        objectImage.setTranslateY(finalY + m_gridYSpaceOffset);
    }

    /**
     * Used to update plant diameter to current size in database.
     * @param xPos necessary because growth moves center of object. Needed to put object back on center.
     * @param yPos necessary because growth moves center of object. Needed to put object back on center.
     * @param diameter current diameter of the plant.
     * @param simObjectID unique id that identifies this object image.
     */
    public void resizePlantImage(double xPos, double yPos, double diameter, int simObjectID) {
        //Searches for object by ID.
        SimObjectView objectImage = findObjectImage(simObjectID, SimType.PLANT);

        //resize image to new size. Expected use is for growing it.
        objectImage.setFitHeight(diameter);
        objectImage.setFitWidth(diameter);

        //Place the object at coordinate again since growth moves it off center.
        moveObjectOnScreen(xPos, yPos, SimType.PLANT, objectImage.getObjectID());
    }

    /**
     * Updates clock text in the GUI.
     */
    public void updateClockText() {
        m_ClockText.setText(m_uI.task.GetHumanReadableTime());
    }

    /**
     * Handles when start button is pressed.
     */
    public void handleStartSimulation() {
        if(m_simHasStarted == false) {
            //Changes background to white at function call.
            m_SimWindow.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));

            //Changes SimStatus states
            m_SimStatusExecution.setBackground(new Background(new BackgroundFill(Color.LIMEGREEN, CornerRadii.EMPTY, Insets.EMPTY)));
            m_SimStatusExecution.setText("Running");
            m_SimStatusClockSpeed.setBackground(new Background(new BackgroundFill(Color.LIMEGREEN, CornerRadii.EMPTY, Insets.EMPTY)));
            m_SimStatusClockSpeed.setText("1x Speed");

            //Creates instance of all other classes before displaying
            m_uI = new UI();

            //First call to display sim to user.
            DisplaySim();

            //Flag that prevents clock from calling GUIController functions before its ready.
            m_readyForClock = true;

            //Prevents this button from doing anything again.
            m_simHasStarted = true;
        }

    }

    /**
     * Handles when stop button is pressed.
     */
    public void handleStopSimulation() {
        //this.m_UI.StopSimulation();
        Platform.exit();
    }

    /**
     * Handles when ChangeSpeeds1x button is pressed.
     */
    public void handleChangeSpeeds1x() {
        if(m_simHasStarted) {
            m_uI.speeds1x();
            m_SimStatusClockSpeed.setText("1x Speed");
        }
    }

    /**
     * Handles when ChangeSpeeds10x button is pressed.
     */
    public void handleChangeSpeeds10x() {
        if(m_simHasStarted) {
            m_uI.speeds10x();
            m_SimStatusClockSpeed.setText("10x Speed");
        }
    }

    /**
     * Handles when handleChangeSpeeds50x button is pressed.
     */
    public void handleChangeSpeeds50x() {
        if(m_simHasStarted) {
            m_uI.speeds50x();
            m_SimStatusClockSpeed.setText("50x Speed");
        }
    }

    /**
     * Handles when handleChangeSpeeds100x button is pressed.
     */
    public void handleChangeSpeeds100x() {
        if(m_simHasStarted) {
            m_uI.speeds100x();
            m_SimStatusClockSpeed.setText("100x Speed");
        }
    }

    /**
     * Handles when Print Report button is pressed.
     */
    public void handlePrintReport() {
        if(m_simHasStarted) {
            this.m_uI.PrintReports();
        }
    }

    /**
     * Handles when Print Report button is pressed.
     */
    public void handlePause() {
        if(m_simHasStarted) {
            if(m_isPaused == false) {
                m_isPaused = true;

                //Update displayed SimStatus labels
                m_SimStatusExecution.setText("Paused");
                m_SimStatusClockSpeed.setBackground(new Background(new BackgroundFill(Color.GRAY, CornerRadii.EMPTY, Insets.EMPTY)));
                m_SimStatusExecution.setBackground(new Background(new BackgroundFill(Color.GRAY, CornerRadii.EMPTY, Insets.EMPTY)));

                //Update button text
                m_PauseButton.setText("Resume");
            }
            else {
                m_isPaused = false;

                //Update displayed SimStatus labels
                m_SimStatusExecution.setText("Running");
                m_SimStatusClockSpeed.setBackground(new Background(new BackgroundFill(Color.LIMEGREEN, CornerRadii.EMPTY, Insets.EMPTY)));
                m_SimStatusExecution.setBackground(new Background(new BackgroundFill(Color.LIMEGREEN, CornerRadii.EMPTY, Insets.EMPTY)));

                //Update button text
                m_PauseButton.setText("Pause");
            }
        }
    }

    /**
     * Prints gui image ID's and database object ID's. Used for debugging purposes.
     */
    public void debugPrintIDs () {
        //Prints GUI object ID's.
        for (SimObjectView object : m_displayedObstaclesList) { // Checks ID's are initialized.
            System.out.println("Gui_ObstacleID: "+object.getObjectID());
        }
        for (SimObjectView object : m_displayedPlantsList) { // Checks ID's are initialized.
            System.out.println("Gui_PlantID: "+object.getObjectID());
        }
        for (SimObjectView object : m_displayedGrazersList) { // Checks ID's are initialized.
            System.out.println("Gui_GrazerID: "+object.getObjectID());
        }
        for (SimObjectView object : m_displayedPredatorsList) { // Checks ID's are initialized.
            System.out.println("Gui_PredatorID: "+object.getObjectID());
        }

        System.out.println();

        //Prints Database object ID's.
        for (Obstacle object : m_dataBase.getV_obstacleList()) { // Checks ID's are initialized.
            System.out.println("Db_ObstacleID: "+object.getO_id());
        }
        for (Plant object : m_dataBase.getV_plantList()) { // Checks ID's are initialized.
            System.out.println("Db_PlantID: "+object.getP_id());
        }
        for (Grazer object : m_dataBase.getV_grazerList()) { // Checks ID's are initialized.
            System.out.println("Db_GrazerID: "+object.UID);
        }
        for (Predator object : m_dataBase.getV_pedatorList()) { // Checks ID's are initialized.
            System.out.println("Db_PredatorID: "+object.UID);
        }

    }

}
