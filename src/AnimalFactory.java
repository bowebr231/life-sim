import java.util.Random;

/**
 * Class that creates all Animals.
 */
public class AnimalFactory
{
    private final LifeSimDataParser parser;
    //These Variables hold teh values for certain Gentic Traits
    double FFspeed;
    double Ffspeed;
    double ffspeed;
    //*****************
    int PredOffspringEnergy;
    int maxNumberofPredOffspring;
    DataBase dataBase;
    private static AnimalFactory animalFactorySingle;
    Random random = new Random();

    double geestationperiodLength;

    /**
     * Constructor that get the values from the parser needed to create the animals
     */
    public AnimalFactory()
   {

       dataBase = DataBase.getInstance();
       parser = LifeSimDataParser.getInstance();
       parser.initDataParser("LifeSimulation01.xml");      //this needs to only be called by one object.
        FFspeed=parser.getPredatorMaxSpeedHOD();
        Ffspeed=parser.getPredatorMaxSpeedHED();
        ffspeed=parser.getPredatorMaxSpeedHOR();
        PredOffspringEnergy = parser.getPredatorOffspringEnergyLevel();
       maxNumberofPredOffspring=parser.getPredatorMaxOffspring();
       geestationperiodLength=parser.getPredatorGestationPeriod();

   }

    /**
     * @return Returns the instance of the factory
     */
   public static AnimalFactory getInstance()
   {
       if(animalFactorySingle == null)
       {
           animalFactorySingle = new AnimalFactory();
       }
       return animalFactorySingle;
   }

    /**
     *Creates all predators that are needed to start the simulation
     * @author Bradley Washburn
     */
    public void CreatePredator()
    {
        //Calculate the predators aggression level and pass it in. This will require that the constructor be rewritten
        int startingPreds = parser.getInitialPredatorCount();
        for(int x = 0; x<startingPreds;x++)
        {
            parser.getPredatorData();
            String genotype = parser.PredatorGenotype;
            //Parsers the genetics into individual char arrays
            char[] aggression = new char[2];
            char[] strength = new char[2];
            char[] speedGenes = new char[2];
            aggression[0] = genotype.charAt(0);
            aggression[1] = genotype.charAt(1);
            strength[0] = genotype.charAt(3);
            strength[1] = genotype.charAt(4);
            speedGenes[0]=genotype.charAt(6);
            speedGenes[1] = genotype.charAt(7);

            Predator predator = new Predator(aggression,calculateSpeed(speedGenes), strength, parser.PredatorX,
                    parser.PredatorY, parser.PredatorEnergy, parser.getPredatorEnergyOutputRate(),
                    parser.getPredatorEnergyToReproduce(), parser.getPredatorMaintainSpeedTime(),
                    parser.getPredatorMaxOffspring(), parser.getPredatorOffspringEnergyLevel(), geestationperiodLength);

            //add predator to the database when needed
            DataBase db = DataBase.getInstance();
            predator.setID(db.PredCount);
            db.PredCount++;

            dataBase.addPredator(predator);
        }


    }

    /**
     * This function must be called for both predators that are reproducing. when you call it the
     * second time flip the order inwhich you pass them in as the order chooses what parent belongs
     * to that offspring.
     * @param pred1 //first Pred reproducing
     * @param pred2 //Second Pred reproducing
     * @author Bradley Washburn
     */
    public void PredatorReproduce(Predator pred1, Predator pred2)
    {
        char[] powerValue;
        double SpeedValue;
        char[] AggresionValue;
        {
            if (Math.random() < 0.5) {
                powerValue = pred1.getP_power();
            } else {
                powerValue = pred2.getP_power();
            }
            if (Math.random() < 0.5) {
                SpeedValue = pred1.getP_speed();
            } else {
                SpeedValue = pred2.getP_speed();
            }
            if (Math.random() < 0.5) {
                AggresionValue = pred1.getP_aggression();
            } else {
                AggresionValue = pred2.getP_aggression();
            }
        }
        //Randomly pick a number of children within the bounds and create that many children
        //for pred1
        int numoffspring = random.nextInt(maxNumberofPredOffspring) +1;
        DataBase db = DataBase.getInstance();
        for(int x=0; x<numoffspring; x++)
        {
            Predator offspring = new Predator(AggresionValue, SpeedValue, powerValue, pred1.X_pos, pred1.Y_pos,
                    PredOffspringEnergy, pred1.movement_EU, pred1.ENERGY_TO_REPRODUCE, pred1.MAINTAIN_SPEED_TIME,
                    pred1.MAX_CHILDREN, pred1.offspring_Energy, geestationperiodLength);


            offspring.setParentID(pred1.UID);
            offspring.setID(db.PredCount);
            db.PredCount++;
            dataBase.addPredatorUnborn(offspring);
            pred1.setChildrenIDs(offspring.UID);
            pred1.inGestation=true;
        }
        //Randomly pick a number of children within the bounds and create that many children
        //for pred2
        numoffspring = random.nextInt(maxNumberofPredOffspring)+1;
        for(int x=0; x<numoffspring; x++) {
            Predator offspring = new Predator(AggresionValue, SpeedValue, powerValue, pred2.X_pos, pred2.Y_pos,
                    PredOffspringEnergy, pred2.movement_EU, pred2.ENERGY_TO_REPRODUCE, pred2.MAINTAIN_SPEED_TIME,
                    pred2.MAX_CHILDREN, pred2.offspring_Energy, geestationperiodLength);
            offspring.setID(db.PredCount);
            offspring.setParentID(pred2.UID);
            //make a list of all the children for this pred that were just made.
            db.PredCount++;
            dataBase.addPredatorUnborn(offspring);
            pred2.setChildrenIDs(offspring.UID);
            pred2.inGestation=true;
        }
    }

    /**
     * Creates the grazers for the start of the sim
     * @author Bradley Washburn
     */
    public void CreateGrazer()
    {
        DataBase db = DataBase.getInstance();
        //db.GrazeCount =+ 1;
        int grazerstartingcount = parser.getInitialGrazerCount();
        for(int x=0;x<grazerstartingcount;x++)
        {
            parser.getGrazerData();
            Grazer grazer = new Grazer(parser.GrazerX, parser.GrazerY,  parser.GrazerEnergy,
                    parser.getGrazerEnergyToReproduce(), parser.getGrazerMaintainSpeedTime(),
                    parser.getGrazerMaxSpeed(), parser.getGrazerEnergyInputRate(),
                    parser.getGrazerEnergyOutputRate(), parser.getGrazerEnergyToReproduce());
            grazer.setID(db.GrazeCount);
            db.GrazeCount++;

            //add it to the database when it is implemented
            dataBase.addGrazer(grazer);
        }

    }


    /**
     * Called to reproduce a grazer
     * @param energy parents current energy level
     * @param xpos x position of the parent
     * @param ypos y position of the parent
     * @author Bradley Washburn
     */
    public void GrazerReproduce(double energy, double xpos, double ypos)     //pass in the grazers current energy and POS
    {
        DataBase db = DataBase.getInstance();
        //These Are the child Grazers from the reproduction process.
        Grazer grazer1 = new Grazer(xpos, ypos, energy/2.0,
                parser.getGrazerEnergyToReproduce(), parser.getGrazerMaintainSpeedTime(),
                parser.getGrazerMaxSpeed(), parser.getGrazerEnergyInputRate(),
                parser.getGrazerEnergyOutputRate(), parser.getGrazerEnergyToReproduce());
        grazer1.setAsNewBorn();
        grazer1.setID(db.GrazeCount);
        db.GrazeCount++;


        Grazer grazer2 = new Grazer(xpos, ypos, energy/2.0,
                parser.getGrazerEnergyToReproduce(), parser.getGrazerMaintainSpeedTime(),
                parser.getGrazerMaxSpeed(), parser.getGrazerEnergyInputRate(),
                parser.getGrazerEnergyOutputRate(), parser.getGrazerEnergyToReproduce());
        grazer2.setID(db.GrazeCount);
        db.GrazeCount++;

        //Add the Grazers to the database
        dataBase.addGrazer(grazer1);
        dataBase.addGrazer(grazer2);
    }

    /**
     * calculates the aggression value of a predator
     * @param aggressionChar //Takes in the aggression string
     * @return returns the numerical value of the aggression gene
     * @author Bradley Washburn
     */
    private double calculateSpeed(char[] aggressionChar)
    {
      if(aggressionChar[0]=='F')
      {
          if(aggressionChar[1]=='F')
          {
              return FFspeed;
          }
          else
          {
              return Ffspeed;
          }
      }
      else
      {
          return ffspeed;
      }
    }
}
